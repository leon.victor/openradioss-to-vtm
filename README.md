# ParaView script to extract parts from OpenRadioss VTK output

The goal of this script is to convert the VTK output of `anim2vtk` to a multiblock structure (vtm file format) where each part is extracted with proper naming.

This script was initially created for the 2024 ParaView Webinar [Advanced rendering of scientific data using ray-tracing in ParaView](https://vimeo.com/915475584).

The part names and IDs are extracted from `.key` files, then the ParaView Extract Selection feature is used to create dedicated block for each part. At the end, all extracted parts are grouped in a multi-block structure and written to disk in VTM file format.

# Usage

Ths script was developed to run with ParaView 5.12.0 that you may download [here](https://www.paraview.org/download/).

In a terminal run:
```
/path/to/paraview/installation/bin/pvpython openradios-to-vtm.py
```

# Improvements

Possible improvements:
 - Use script command line arguments for key file and vtk input files
 - Handle several key files
 - Change output to [VTKHDF file format](https://docs.vtk.org/en/latest/design_documents/VTKFileFormats.html#vtkhdf-file-format) with transient support and use static mesh to reduce file size

# Contact

For any requests or questions, please contact Kitware using [the contact page](https://www.kitware.eu/contact/).

# License
Copyright (c) Kitware 2024

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
