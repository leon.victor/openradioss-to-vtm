# SPDX-FileCopyrightText: Copyright (c) Kitware
# SPDX-License-Identifier: Apache-2.0

import paraview
from paraview.simple import *

key_filepath='camry-detailed-v5a.key'
vtk_filenames=['CamryOpenRadiossA001',
               'CamryOpenRadiossA002',
               'CamryOpenRadiossA003',
               'CamryOpenRadiossA004',
               'CamryOpenRadiossA005',
               'CamryOpenRadiossA006',
               'CamryOpenRadiossA007',
               'CamryOpenRadiossA008',
               'CamryOpenRadiossA009',
               'CamryOpenRadiossA010',
               'CamryOpenRadiossA011',
               'CamryOpenRadiossA012',
               'CamryOpenRadiossA013',
               'CamryOpenRadiossA014',
               'CamryOpenRadiossA015',
               'CamryOpenRadiossA016',
               'CamryOpenRadiossA017',
               'CamryOpenRadiossA018',
               'CamryOpenRadiossA019',
               'CamryOpenRadiossA020',
               'CamryOpenRadiossA021',
               'CamryOpenRadiossA022',
               'CamryOpenRadiossA023',
               'CamryOpenRadiossA024',
               'CamryOpenRadiossA025']
input_folder='output_vtk'
output_folder='output_mb'

partnames_by_id = dict()

with open(key_filepath) as key_file:
    is_in_part_section_name = False
    part_name = ''
    is_in_part_section_id = False
    part_id=''
    for line in key_file:
        if(line.startswith('$')):
            continue
        if(is_in_part_section_name):
            part_name = line.split()[0]
            is_in_part_section_name = False
            is_in_part_section_id = True
        elif(is_in_part_section_id):
            part_id = line.split()[0]
            is_in_part_section_name = False
            is_in_part_section_id = False
            partnames_by_id[part_id] = part_name
        elif('*PART' in line):
            is_in_part_section_name = True
            is_in_part_section_id = False

for vtk_filename in vtk_filenames:
    print(f'Processing {vtk_filename}...')
    camryvtu = LegacyVTKReader(registrationName='camryvtu', FileNames=[f'{input_folder}/{vtk_filename}.vtk'])

    extracted_proxies = []
    for id in partnames_by_id:
        part_name = partnames_by_id[id]
        selection_sources = CreateSelection(proxyname='SelectionQuerySource', registrationname='selection_sources', QueryString=f'(PART_ID == {id})',Assembly='',Selectors=['/'])
        selection_filter = CreateSelection(proxyname='AppendSelections', registrationname='selection_filter', Input=selection_sources, Expression='s0', SelectionNames=['s0'])
        extracted_proxies.append(ExtractSelection(registrationName=part_name, Input=camryvtu, Selection=selection_filter))

    groupDatasets1 = GroupDatasets(registrationName='GroupDatasets1', Input=extracted_proxies)
    SaveData(f'{output_folder}/{vtk_filename}.vtm', proxy=groupDatasets1, PointDataArrays=['Acceleration', 'Contact_Forces', 'Contact_Pressure_/_Normal', 'Contact_Pressure_/_Tangent', 'Displacement', 'Mass_Change', 'NODE_ID', 'Tied_Contact_Forces', 'Time_Step', 'Velocity'], CellDataArrays=['1DELEM_Plastic_Strain', '1DELEM_Specific_Energy', '2DELEM_Plastic_Strain', '2DELEM_Plastic_Strain_Lower', '2DELEM_Plastic_Strain_Upper', '2DELEM_Specific_Energy', '2DELEM_Stress_(lower)', '2DELEM_Stress_(upper)', '3DELEM_Plastic_Strain', '3DELEM_Specific_Energy', '3DELEM_Stress', 'ELEMENT_ID', 'EROSION_STATUS', 'PART_ID'], FieldDataArrays=['CYCLE', 'TIME'], Assembly='Hierarchy')

